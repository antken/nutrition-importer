# README #

### What is this repository for? ###

* Project parses nutrition data from external source and saves it to a database
* version 0.0.1

### How do I get set up? ###

* Clone project into local repository
* Add external resources URLs into resources folder
* Add hibernate.cfg.xml file with your database 
* Run maven clean install (this will run all tests too)

### Who do I talk to? ###

* Anton Fofanov