package main.domain;

public class NutritionPortion {

    private Nutrition nutrition;
    private double amount;
    private String unit;

    public Nutrition getNutrition() {
	return nutrition;
    }

    public void setNutrition(Nutrition nutrition) {
	this.nutrition = nutrition;
    }

    public double getAmount() {
	return amount;
    }

    public void setAmount(double amount) {
	this.amount = amount;
    }

    public String getUnit() {
	return unit;
    }

    public void setUnit(String unit) {
	this.unit = unit;
    }

    @Override
    public String toString() {
	return "NutritionPortion{" +
		"nutrition=" + nutrition +
		", amount=" + amount +
		", unit='" + unit + '\'' +
		'}';
    }
}
