package main.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table
public class Nutrition {

    @Id
    @GeneratedValue
    @Column(name = "nutrition_id")
    @JsonIgnore
    private Long id;

    @Column(name = "full_name", nullable = false)
    private String fullName;
    @JsonIgnore
    private int sourceId;
    @JsonIgnore
    private double protein;
    @JsonIgnore
    private double carb;
    @JsonIgnore
    private double totalFat;
    @JsonIgnore
    private double saturatedFat;
    @JsonIgnore
    private double monounsaturatedFat;
    @JsonIgnore
    private double polyunsaturatedFat;
    @JsonIgnore
    private double sodium;
    @JsonIgnore
    private double kcal;
    @JsonIgnore
    private double fiber;
    @JsonIgnore
    private double sugar;
    @JsonIgnore
    private String source;

    public Nutrition() {
    }

    public Nutrition(String fullName, int sourceId, String source) {
	this.fullName = fullName;
	this.sourceId = sourceId;
	this.source = source;
    }

    public String getFullName() {
	return fullName;
    }

    public void setFullName(String fullName) {
	this.fullName = fullName;
    }

    public int getSourceId() {
	return sourceId;
    }

    public void setSourceId(int sourceId) {
	this.sourceId = sourceId;
    }

    public double getProtein() {
	return protein;
    }

    public void setProtein(double protein) {
	this.protein = protein;
    }

    public double getCarb() {
	return carb;
    }

    public void setCarb(double carb) {
	this.carb = carb;
    }

    public double getTotalFat() {
	return totalFat;
    }

    public void setTotalFat(double totalFat) {
	this.totalFat = totalFat;
    }

    public double getSaturatedFat() {
	return saturatedFat;
    }

    public void setSaturatedFat(double saturatedFat) {
	this.saturatedFat = saturatedFat;
    }

    public double getMonounsaturatedFat() {
	return monounsaturatedFat;
    }

    public void setMonounsaturatedFat(double monounsaturatedFat) {
	this.monounsaturatedFat = monounsaturatedFat;
    }

    public double getPolyunsaturatedFat() {
	return polyunsaturatedFat;
    }

    public void setPolyunsaturatedFat(double polyunsaturatedFat) {
	this.polyunsaturatedFat = polyunsaturatedFat;
    }

    public double getSodium() {
	return sodium;
    }

    public void setSodium(double sodium) {
	this.sodium = sodium;
    }

    public double getKcal() {
	return kcal;
    }

    public void setKcal(double kcal) {
	this.kcal = kcal;
    }

    public double getFiber() {
	return fiber;
    }

    public void setFiber(double fiber) {
	this.fiber = fiber;
    }

    public double getSugar() {
	return sugar;
    }

    public void setSugar(double sugar) {
	this.sugar = sugar;
    }

    public Long getId() {
	return id;
    }

    public void setId(Long id) {
	this.id = id;
    }

    public String getSource() {
	return source;
    }

    public void setSource(String source) {
	this.source = source;
    }

    @Override
    public String toString() {
	return "Nutrition [fullName=" + fullName + "]";
    }

}
