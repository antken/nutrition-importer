package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import main.domain.Nutrition;
import main.parse.ACParser;
import main.parse.IParsing;
import main.parse.URLsResource;
import main.store.HibernateSaver;
import main.store.INutritionsSaving;

public class FoodImporter {
    private static final Logger LOG = Logger.getLogger(FoodImporter.class);
    protected IParsing parser;
    protected INutritionsSaving saver;
    protected URLsResource parsingURLs;

    public FoodImporter(IParsing parser, INutritionsSaving saver) {
	this.parser = parser;
	this.saver = saver;
	this.parsingURLs = new URLsResource();
    }

    public static void main(String[] args) {
	FoodImporter importer = new FoodImporter(new ACParser(), new HibernateSaver());
	importer.save(importer.parse());
	LOG.info("Finished food import.");
    }

    public List<Nutrition> parse() {
	List<Nutrition> parsedNutritions = new ArrayList<>();
	parsedNutritions.addAll(parseSource(parsingURLs.loadProteinSources(), 1));
	parsedNutritions.addAll(parseSource(parsingURLs.loadFatSources(), 2));
	parsedNutritions.addAll(parseSource(parsingURLs.loadCarbSources(), 3));
	parsedNutritions.addAll(parseSource(parsingURLs.loadVeggieSources(), 4));
	return parsedNutritions;
    }

    public void save(List<Nutrition> nutritions) {
	LOG.info("Saving ");
	saver.saveAll(nutritions);
	saver.shutDownConnection();
    }

    private List<Nutrition> parseSource(List<String> urls, int sourceId) {
	List<Nutrition> nutritions = new ArrayList<>();
	for (String url : urls) {
	    try {
		LOG.info("Parsing " + url);
		nutritions.add(parser.parseFrom(url, sourceId));
	    } catch (IOException e) {
		LOG.error("Could not import a nutrition from url:" + url, e);
	    }
	}
	return nutritions;
    }
}
