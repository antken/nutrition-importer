package main.parse;

import java.io.IOException;

import main.domain.Nutrition;

public interface IParsing {
    public Nutrition parseFrom(String url, int sourceId) throws IOException;

}
