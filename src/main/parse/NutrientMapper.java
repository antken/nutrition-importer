package main.parse;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;

import org.apache.log4j.Logger;

import main.domain.Nutrition;

public class NutrientMapper {
    private static final Logger LOG = Logger.getLogger(NutrientMapper.class);
    protected static final Map<String, BiConsumer<Nutrition, Double>> MAPPERS;

    private NutrientMapper() {
    }

    static {
	MAPPERS = new HashMap<>();
	MAPPERS.put("protein", (n, v) -> n.setProtein(v));
	MAPPERS.put("total lipid (fat)", (n, v) -> n.setTotalFat(v));
	MAPPERS.put("carbohydrate, by difference", (n, v) -> n.setCarb(v));
	MAPPERS.put("fiber, total dietary", (n, v) -> n.setFiber(v));
	MAPPERS.put("sodium, na", (n, v) -> n.setSodium(v));
	MAPPERS.put("fatty acids, total saturated", (n, v) -> n.setSaturatedFat(v));
	MAPPERS.put("fatty acids, total monounsaturated", (n, v) -> n.setMonounsaturatedFat(v));
	MAPPERS.put("fatty acids, total polyunsaturated", (n, v) -> n.setPolyunsaturatedFat(v));
	MAPPERS.put("energy", (n, v) -> n.setKcal(v));
	MAPPERS.put("sugars, total", (n, v) -> n.setSugar(v));
    }

    public static BiConsumer<Nutrition, Double> getMapper(String nutrientName) {
	return MAPPERS.getOrDefault(nutrientName.toLowerCase(),
		(n, v) -> logWarning(n.getFullName(), nutrientName));
    }

    private static void logWarning(String nutritionName, String nutrientName) {
	LOG.warn("unknown nutrient type <" + nutrientName + "> for " + nutritionName);
    }
}
