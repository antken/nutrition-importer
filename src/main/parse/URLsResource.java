package main.parse;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.stream.Collectors;

public class URLsResource {
    private static final String PROTEIN_SOURCES = "proteinresource";
    private static final String CARB_SOURCES = "carbresource";
    private static final String FAT_SOURCES = "fatresource";
    private static final String VEGGIE_SOURCES = "vegetableresource";

    public List<String> loadProteinSources() {
	return fetchListOfURLs(PROTEIN_SOURCES);
    }

    public List<String> loadCarbSources() {
	return fetchListOfURLs(CARB_SOURCES);
    }

    public List<String> loadFatSources() {
	return fetchListOfURLs(FAT_SOURCES);
    }

    public List<String> loadVeggieSources() {
	return fetchListOfURLs(VEGGIE_SOURCES);
    }

    private List<String> fetchListOfURLs(String file) {
	InputStream stream = this.getClass().getClassLoader().getResourceAsStream(file);

	return new BufferedReader(new InputStreamReader(stream))
		.lines()
		.collect(Collectors.toList());
    }
}