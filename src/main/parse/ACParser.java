package main.parse;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.function.Predicate;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import main.domain.Nutrition;

public class ACParser implements IParsing {
    private static final long LINE_WITH_FULL_NAME = 4;
    private static final long LINES_WITH_ALL_HEADERS = 6;
    private static final String IGNORE_NUTRITION_ID = "Nutrient data for:";
    private static final String IGNORE_HEADER = "raw";
    private static final String SEPARATOR = ",";

    @Override
    public Nutrition parseFrom(String url, int sourceId) throws IOException {
	Reader reader = new InputStreamReader(new URL(url).openStream(), StandardCharsets.ISO_8859_1);
	List<CSVRecord> records = CSVFormat.EXCEL.parse(reader).getRecords();

	return parseLineByLine(records, sourceId, url);
    }

    private Nutrition parseLineByLine(List<CSVRecord> records, int sourceId, String url) throws IOException {
	Nutrition nutrition = null;

	for (CSVRecord record : records) {
	    if (record.getRecordNumber() == LINE_WITH_FULL_NAME) {
		nutrition = new Nutrition(getFullNameFromRecord(record), sourceId, url);
	    } else if (record.getRecordNumber() > LINES_WITH_ALL_HEADERS) {
		addNutrientFromRecord(nutrition, record);
	    }
	}
	return nutrition;

    }

    private String getFullNameFromRecord(CSVRecord record) {
	StringBuilder sb = new StringBuilder();
	String[] splitted = record.iterator()
		.next()
		.split(SEPARATOR);

	Arrays.asList(splitted).stream()
		.filter(shouldBePartOfName())
		.forEach(sb::append);

	return sb.toString();
    }

    private Predicate<? super String> shouldBePartOfName() {
	return s -> !s.contains(IGNORE_HEADER) && !s.contains(IGNORE_NUTRITION_ID);
    }

    private void addNutrientFromRecord(Nutrition nutrition, CSVRecord record) {
	Iterator<String> it = record.iterator();
	String nutrientName = it.next();

	while (it.hasNext()) {
	    try {
		mapValueToNutrient(nutrition, nutrientName, it.next());
		break;
	    } catch (NumberFormatException e) {
		// try to map the first value (per 100g). Ignore errors
	    }
	}
    }

    private void mapValueToNutrient(Nutrition nutrition, String nutrientName, String recordValue) {
	double value = Double.parseDouble(recordValue.trim());
	NutrientMapper.getMapper(nutrientName).accept(nutrition, value);
    }
}
