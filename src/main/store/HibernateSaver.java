package main.store;

import java.util.List;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import main.domain.Nutrition;

public class HibernateSaver implements INutritionsSaving {
    private static final int SELECT_ONE_RESULT = 1;
    private static final String VALIDATION_QUERY = "SELECT " + SELECT_ONE_RESULT;

    @Override
    public void saveAll(List<?> items) {
	Session session = HibernateUtil.getSession();
	session.beginTransaction();
	items.forEach(session::save);
	session.getTransaction().commit();
    }

    @Override
    public List<Nutrition> getAll() {
	Session session = HibernateUtil.getSession();
	final CriteriaQuery<Nutrition> criteria = session.getCriteriaBuilder().createQuery(Nutrition.class);
	final Root<Nutrition> allEntries = criteria.from(Nutrition.class);

	return session.createQuery(criteria.select(allEntries)).getResultList();
    }

    @Override
    public void shutDownConnection() {
	HibernateUtil.shutdown();
    }

    @Override
    public boolean ping() {
	Session session = HibernateUtil.getSession();
	final int size = session.createQuery(VALIDATION_QUERY).getResultList().size();
	return size == SELECT_ONE_RESULT;
    }

    @Override
    public List<Nutrition> getMany(List<Long> ids) {
	Session session = HibernateUtil.getSession();
	return session.byMultipleIds(Nutrition.class).multiLoad(ids);
    }

}
