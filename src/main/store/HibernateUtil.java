package main.store;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {
    private static final Logger LOG = Logger.getLogger(HibernateUtil.class);
    private static final String CONFIG_FILE = "hibernate.cfg.xml";
    private static final SessionFactory SESSION_FACTORY = buildSessionFactory();
    private static final Session SESSION = SESSION_FACTORY.openSession();

    private HibernateUtil() {
    }

    private static SessionFactory buildSessionFactory() {
	try {
	    return new Configuration().configure(CONFIG_FILE).buildSessionFactory();
	} catch (Exception e) {
	    LOG.error("Initial SessionFactory creation failed." + e);
	    throw new ExceptionInInitializerError(e);
	}
    }

    public static SessionFactory getSessionFactory() {
	return SESSION_FACTORY;
    }

    public static Session getSession() {
	return SESSION;
    }

    public static void shutdown() {
	getSession().close();
	getSessionFactory().close();
    }
}
