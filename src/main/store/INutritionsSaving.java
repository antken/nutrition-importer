package main.store;

import java.util.List;

import main.domain.Nutrition;

public interface INutritionsSaving {
    public void shutDownConnection();

    public boolean ping();

    public void saveAll(List<?> items);

    public List<Nutrition> getAll();

    public List<Nutrition> getMany(List<Long> ids);
}
